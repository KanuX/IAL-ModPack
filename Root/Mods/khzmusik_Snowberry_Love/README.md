# Snowberry Love

In the vanilla game, snowberries are nearly usesless.
They cannot be grown, their only use is to eat them, and they don't give much food.

This seems like a waste to me, so I added a bunch of uses for snowberries.

## Features
In the real world, snowberries were used mostly for medicinal purposes by Native American tribes.
They also taste like wintergreen (reportedly - I have not eaten them).
I based the in-game uses on these facts.*

* Snowberries can be grown from seeds and harvested.
  The seed recipe is unlocked from either a schematic,
  or from the same Living Off The Land perk level as blueberry seeds.
* Snowberry tea can be crafted in a campfire with a cooking pot.
  The tea gives 24 water and cures dysentery 10%.
* Snowberry paste can be used the same as medical aloe cream,
  and can be used instead of aloe cream when crafting first aid bandages.
  The paste takes five snowberries to craft.
* Snowberries replace blueberries in herbal antibiotics recipes.
* Grandma's Gin: "How intelligent people get drunk and belligerent."
  It provides the buffs of beer and the nerdy glasses, combined.
  The gin recipe is unlocked at the same crafting level as Grandpa's Moonshine.

\* References:
* [Wikipedia](https://en.wikipedia.org/wiki/Symphoricarpos#Cultivation_&_Medicinal_Uses)
* USDA [Plant of the Week: Creeping Snowberry](https://www.fs.usda.gov/wildflowers/plant-of-the-week/gaultheria_hispidula.shtml)
* USDA [Snowberry Plant Fact Sheet](https://plants.usda.gov/DocumentLibrary/factsheet/pdf/fs_syal.pdf) (PDF)

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.

However, the modlet also includes new non-XML resources
(new icons).
These resources are _not_ pushed from server to client.
For this reason, the modlet should be installed on both servers and clients.

### Re-use of new assets

The icon images are derivative works of The Fun Pimps original images.
I do *not* claim any rights over these images.

You may only re-use the images under the same terms and conditions that you would
use the original images from The Fun Pimps.

The new "Grandma's Gin" icon also incorporates this Gin Bottle image from Wikimedia Commons:
https://commons.wikimedia.org/wiki/File:Bottle,_gin_(AM_616677-5).jpg

Image by Udolpho Wolfe, released as CC-BY by by Auckland Museum.

If you want to re-use this mod without that credit,
the old image is still in the `UIAtlases/ItemIconAtlas` directory.
It only uses assets from The Fun Pimps.
