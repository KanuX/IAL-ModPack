# Durable Nests and Trash

[![🧪 Tested On](https://img.shields.io/badge/🧪%20Tested%20On-A21%20b317-blue.svg)](https://7daystodie.com/) [![📦 Automated Release](https://github.com/jonathan-robertson/durable-nests-and-trash/actions/workflows/release.yml/badge.svg)](https://github.com/jonathan-robertson/durable-nests-and-trash/actions/workflows/release.yml)

- [Durable Nests and Trash](#durable-nests-and-trash)
  - [Summary](#summary)
    - [Support](#support)

## Summary

A very simple mod that keeps nests and trash bags from breaking when they are looted.

This restores the behavior these loot containers displayed before A21.

### Support

🗪 If you would like support for this mod, please feel free to reach out via [Discord](https://discord.gg/tRJHSB9Uk7).
