# TextureVultureRadiated
## About
* **Need EAC Disable**
* Added texture to vulture radiated.


## Changelog
### 2023.6/14 ver 0.1
* Patch for A21 (b313)
* Added texture to vulture radiated.
* Initial version