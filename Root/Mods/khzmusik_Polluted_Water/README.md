# Polluted Water

All water from natural sources is polluted and must be purified.

## Features

* All natural water sources yield polluted water instead of murky water.
    Polluted water takes away 15 health, and has a near-certain chance of giving you dysentery.
    Polluted water can _not_ be boiled, and must be purified to murky water.
* Adds water purification tablets.
    Tablets can be crafted in either a campfire with a beaker,
    or a chemistry station.
    The recipe unlocks with the recipe for XXX.
    They are also a rare find in medical loot.
* Purification tablets require bleach, which is new to the game.
    Bleach cannot be crafted, but can be found in loot in the same places as acid.
* Glass jars are back, but can only be filled with polluted water.
    Jars can be looted and crafted, but are **not** returned when liquids are consumed.
    This is for compatibility with other consumables in A21, and for balance reasons.
* Snowballs can be turned into polluted water using a glass jar at a campfire.
* Includes the features from the [Murky Dew](../khzmusik_Murky_Dew) modlet,
    where dew collectors yield murky water but do not produce heat.
    _You do not need to install that modlet separately._

## Technical Details

This modlet uses XPath to modify XML files, and does not have custom C# code.
It should be compatible with EAC.

However, the modlet also includes new non-XML resources
(new icons).
These resources are _not_ pushed from server to client.
For this reason, the modlet should be installed on both servers and clients.

### Re-use of new assets

Most of the icon images are derivative works of The Fun Pimps original images.
I do *not* claim any rights over these images.

You may only re-use the images under the same terms and conditions that you would
use the original images from The Fun Pimps.

However, the bleach bottle icon may be used freely.
The bottle itself is from a photo I took.
The logo comes from a bleach bottle image that is released under a CC0 (public domain) license:
https://openclipart.org/detail/29000/bleach-bottle

The new icons were created using Gimp.
The Gimp source files are included in the `UIAtlases/ItemIconAtlas` folder,
alongside the icons themselves.
