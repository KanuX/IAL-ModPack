# LessGrass
## About
* **Need EAC Disable**
* Less grass is generated in biomes.


## Changelog
### 2023.6/14 ver 0.1
* Patch for A21 (b313)
* Less grass is generated in biomes.
* Initial version