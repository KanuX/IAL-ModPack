# SpoiledMeat
## About
* **Need EAC Disable**
* Added a recipe to spoiled the raw meat.


## Changelog
### 2023.6/14 ver 0.1
* Patch for A21 (b313)
* Added a recipe to spoiled the raw meat.
* Initial version