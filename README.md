IAL-ModPack
===========

Just some mods to tweak the Alpha 21 of 7 Days to Die.

Download
--------

<p align="center">
  <a href="https://gitlab.com/KanuX/IAL-ModPack/-/archive/master/IAL-ModPack-master.zip">
    <img src="/.media/download_zip.png" width="256" title="Download ZIP archive">
  </a>
  <a href="https://gitlab.com/KanuX/IAL-ModPack/-/archive/master/IAL-ModPack-master.tar">
    <img src="/.media/download_tar.png" width="256" title="Download TAR archive">
  </a>
</p>

Install
-------

Move the `IAL-ModPack-master` folder contents to 7 Days to Die directory.

- Steam:

  * Move contents of `Root` folder here:

  ```shell
  SteamLibrary/steamapps/common/7 Days to Die/Mods
  ```
  * Move contents of `Data` folder here:

  <details><summary>Windows</summary>

  ```batch
  C:/User/USERNAME/AppData/Roaming/7DaysToDie/
  ```
  or
  ```batch
  %AppData%/Roaming/7DaysToDie/
  ```

  </details>

  <details><summary>Linux</summary>

  ```shell
  $HOME/.local/share/7DaysToDie/
  ```

  </details>

Mods and Worlds
---------------

- Generated Worlds:

|                             Name                              |                                        URL                                        |
|---------------------------------------------------------------|-----------------------------------------------------------------------------------|
| New York Undead 21                                            | https://7daystodiemods.com/map-new-york-undead-21/                                |
| Subway-Patch for “New York Undead 21”                         | https://7daystodiemods.com/subway-patch-for-new-york-undead-21/                   |

- Mods:

|                             Name                              |                                       URL                                         |
|---------------------------------------------------------------|-----------------------------------------------------------------------------------|
| Hybrid Learn by Use (Action Skills)                           | https://7daystodiemods.com/hybrid-learn-by-use-action-skills/                     |
| The Sponsorship of Darkness                                   | https://7daystodiemods.com/the-sponsorship-of-darkness/                           |
| Redbeardt’s Path Smoothing                                    | https://7daystodiemods.com/redbeardts-path-smoothing/                             |
| Realistic Guns Overhaul                                       | https://7daystodiemods.com/realistic-guns-overhaul/                               |
| Vanilla Drink Models                                          | https://7daystodiemods.com/vanilla-drink-models-a21/                              |
| Apocalyptic Quests | https://7daystodiemods.com/apocalyptic-quests/ |
| Visual Level | https://7daystodiemods.com/visual-level/ |
| Protective Vehicles | https://7daystodiemods.com/protective-vehicles-a21/ |
| Lt. Dan’s A21 Doors Revision | https://7daystodiemods.com/lt-dans-a21-doors-revision/ |
| Add Leaping Lizards | https://7daystodiemods.com/add-leaping-lizards/ |
| Advanced Junk Turret | https://7daystodiemods.com/advanced-junk-turret/ |
| Ratbertt’s A21 Quality of Life | https://7daystodiemods.com/ratbertts-a21-quality-of-life/ |
| Honey Crafting | https://7daystodiemods.com/honey-crafting/ |
| Craft Climbing Rope | https://7daystodiemods.com/craft-climbing-rope/ |
| BreachingCharge+ | https://7daystodiemods.com/breachingcharge/ |
| More Craftables Secure Storage | https://7daystodiemods.com/more-craftables-secure-storage/ |
| HD Truck Elevator | https://7daystodiemods.com/hd-truck-elevator/ |
| HD Generator | https://7daystodiemods.com/hd-generator/ |
| HD Beehive | https://7daystodiemods.com/hd-beehive/ |
| HD Pump Jack | https://7daystodiemods.com/hd-pump-jack/ |
| Black Wolf’s Better Fire | https://7daystodiemods.com/black-wolfs-better-fire-molotovs-and-fire-arrows-darts/ |
| Zombie Tracker | https://7daystodiemods.com/zombie-tracker/ |
| Escape From Tarkov Sounds | https://7daystodiemods.com/escape-from-tarkov-sounds/ |
| Boating Mod | https://7daystodiemods.com/boating-mod/ |
| Solar Powered | https://7daystodiemods.com/solar-powered/ |
| STYX Steel Ammo With Loot | https://7daystodiemods.com/styx-steel-ammo-with-loot/ |
| Almost Universal Turret Ammo Mod | https://7daystodiemods.com/almost-universal-turret-ammo-mod/ |
| Scrap Polymer Recipe | https://7daystodiemods.com/scrap-polymer-recipe/ |
| Water Filter Recipe | https://7daystodiemods.com/water-filter-recipe/ |
| Banshee Queen | https://7daystodiemods.com/banshee-queen/ |
| STYX Color Fix Mod Ramos Recipe Enough | https://7daystodiemods.com/styx-color-fix-mod-ramos-recipe-enough/ |
| More Lead and Brass | https://7daystodiemods.com/more-lead-and-brass/ |
| Acid Rain | https://7daystodiemods.com/acid-rain/ |
| Compact Dew Collector | https://7daystodiemods.com/compact-dew-collector/ |
| Nicer Bird Nests | https://7daystodiemods.com/nicer-bird-nests/ |
| Clinoptilolite Ammo (Remove Rad Healing Ammo) | https://7daystodiemods.com/clinoptilolite-ammo-remove-rad-healing-ammo/ |
| Incendiary Ammo | https://7daystodiemods.com/incendiary-ammo/ |
| Better Traps | https://7daystodiemods.com/better-traps/ |
| Night Vision Helmet Mod | https://7daystodiemods.com/night-vision-helmet-mod/ |
| Zombie Wraith | https://7daystodiemods.com/zombie-wraith/ |
| Animal Overhauls | https://7daystodiemods.com/animal-overhauls/ |
| More Containers | https://7daystodiemods.com/more-containers/ |
| Alternative Ammo Icons | https://7daystodiemods.com/alternative-ammo-icons/ |
| Reset Quests | https://7daystodiemods.com/reset-quests/ |
| Steel Spike Traps | https://7daystodiemods.com/steel-spike-traps/ |
| Better Vehicles | https://7daystodiemods.com/better-vehicles/ |
| Automated Mining and Ammunition Making Robotic Workstations | https://7daystodiemods.com/automated-mining-and-ammunition-making-robotic-workstations/ |
| Ghillie Suit Mods | https://7daystodiemods.com/ghillie-suit-mods/ |
| SMG Stock | https://7daystodiemods.com/smg-stock/ |
| BK Sniper Turret | https://7daystodiemods.com/bk-sniper-turret-a21/ |
| Ratbertts Radiated Wastelands | https://7daystodiemods.com/ratbertts-radiated-wastelands/ |
| Loot Bag Timer | https://7daystodiemods.com/loot-bag-timer/ |
| Add Food and Signs | https://7daystodiemods.com/add-food-and-signs/ |
| Add Candy | https://7daystodiemods.com/add-candy/ |
| Pickup the New Flags and Posters | https://7daystodiemods.com/pickup-the-new-flags-and-posters/ |
| MD Advanced Generator | https://7daystodiemods.com/md-advanced-generator-a21/ |
| Pickup Turrets | https://7daystodiemods.com/pickup-turrets-a21/ |
| Bigger Backpack Mod (60/96 Slot) | https://7daystodiemods.com/bigger-backpack-mod-60-96-slot/ |
| Food and Water Bars | https://7daystodiemods.com/food-and-water-bars/ |
| Polluted Water | https://7daystodiemods.com/polluted-water/ |
| Working Decor | https://7daystodiemods.com/working-decor-a21/ |
| Additional Arrows | https://7daystodiemods.com/additional-arrows/ |
| Snowberry Love | https://7daystodiemods.com/snowberry-love/ |
| Quiet Nailgun | https://7daystodiemods.com/quiet-nailgun/ |
| Mod Schematics | https://7daystodiemods.com/mod-schematics/ |
| Donovan Craftable Parts | https://7daystodiemods.com/donovan-craftable-parts/ |
| No Heat Dew Collector | https://7daystodiemods.com/no-heat-dew-collector/ |
| Better Bridges | https://7daystodiemods.com/better-bridges/ |
| Lootable Zombie Corpse | https://7daystodiemods.com/lootable-zombie-corpse-a21/ |
| Doughs Weather Core | https://7daystodiemods.com/doughs-weather-core/ |
| EliteZombies | https://7daystodiemods.com/elitezombies/ |
| Durable Nests and Trash | https://7daystodiemods.com/durable-nests-and-trash/ |
| Craftable Hazmat | https://7daystodiemods.com/craftable-hazmat-a21/ |
| Remove Environmental Tooltips | https://7daystodiemods.com/remove-environmental-tooltips/ |
| Add Beaker | https://7daystodiemods.com/add-beaker/ |
| Add Vitamin | https://7daystodiemods.com/add-vitamin/ |
| Not Just a Cosmetic | https://7daystodiemods.com/not-just-a-cosmetic/ |
| Harvesting Crops Will Always Give a Seed | https://7daystodiemods.com/harvesting-crops-will-always-give-a-seed/ |
| GK Boxes Back | https://7daystodiemods.com/gk-boxes-back/ |
| Less Grass | https://7daystodiemods.com/less-grass/ |
| GK Spoiled Meat | https://7daystodiemods.com/gk-spoiled-meat/ |
| GK Storage | https://7daystodiemods.com/gk-storage/ |
| GK Texture Vulture Radiated | https://7daystodiemods.com/gk-texture-vulture-radiated/ |
| Zombie Reach Adjustment | https://7daystodiemods.com/zombie-reach-adjustment/ |
| Not Just for Clubs | https://7daystodiemods.com/not-just-for-clubs/ |
| Log Spikes | https://7daystodiemods.com/log-spikes/ |
| Home Sweet Home | https://gitlab.com/moredaystodie/home-sweet-home |
| Medicine Plus | https://gitlab.com/moredaystodie/medicine-plus |
| No Weapon Crosshair | https://gitlab.com/moredaystodie/no-weapon-crosshair |
| Trader Quest Addendum | https://7daystodiemods.com/trader-quest-addendum/ |

Fixes and Modifications
-----------------------

There are no fixes or modifications available.

